(function () {
  'use strict';


  $('.comments').cycle({
    fx: 'scrollHorz',
    timeout:0,
    speed: 600,
    slides: '> .comment',
    pager: '.bullets'
  });

  $('.pricing-slider').cycle({
    fx: 'scrollHorz',
    timeout:0,
    speed: 600,
    slides: '> .slide',
    pager: '#pager-plan'
  });

  $('.plan').on('click', function() {
    $('.plan').removeClass('active-plan');

    $(this).addClass('active-plan');
    $('.btn-hire').addClass('active open-hiring-box').html('Contratar plano');
  });

  $('.open-hiring-box').on('click', function(e) {
    e.preventDefault();

    // Pegar o plano ativo
    var planName = $('.active-plan h5').html();
    console.log(planName);
    var checkBox = $('.active-plan').parent().find('.checkboxInput');

    if(checkBox.is(':checked')) {
      $('.my-plan').val(planName + ' + Suporte de Especialista');
    } else {
      $('.my-plan').val(planName);
    }

    // FIM DE PEGAR O PLANO ATIVO


    $('html, body').animate({
      scrollTop: $("#header").offset().top
    }, 800);
    $('.pricing-content').addClass('opened-hiring');
    $('.hiring').addClass('opened-hiring-box');
  });

  $('.back-plan').on('click', function(e) {
    e.preventDefault();
    $('.pricing-content').removeClass('opened-hiring');
    $('.hiring').removeClass('opened-hiring-box');
  });


  // COISA DO FAQ
  $('.open-faq').on('click', function(e) {
    e.preventDefault();

    var faqQuestions = $('.faq-questions');

    if( faqQuestions.hasClass('faq-shown') ) {
      faqQuestions.removeClass('faq-shown');
      $('.question').removeClass('question-shown');
      $(this).removeClass('active');
    } else {
      faqQuestions.addClass('faq-shown');
      $(this).addClass('active');
    }
  })

  $('.question-title').on('click', function() {

    $('.question').removeClass('question-shown');
    var blockQuestion = $(this).parent();

    if( blockQuestion.hasClass('question-shown') ) {
      blockQuestion.removeClass('question-shown');
    } else {
      blockQuestion.addClass('question-shown');
    }

  })

  // FORMULÁRIO DE CONTATO
  $('#contact-form').on('submit', function(e) {
    e.preventDefault();

    $(this).find('.required').each(function() {
      if ( $(this).val() == '' ){
        $(this).addClass('erro');
        // setTimeout(function(){ $('.required').removeClass('erro') }, 1000);
      }
    });

    if ( $(this).find('.erro').length == 0 ) {
      $('.msg-success').fadeIn();
      $('.msg-error-input').fadeOut();
      setTimeout(function(){ $('.msg-success').fadeOut(); }, 3000);

      var data = $(this).serialize();
      $.ajax({
        type: 'post',
        url: 'send.php',
        data: data,
        success: function(resp) {
          console.dir(resp);
        }
      });

      $('input, textarea, select').val('');


    } else {
      $('.msg-error-input').fadeIn();
      $('.msg-success').fadeOut();
      // setTimeout(function(){ $('.msg-error-input').fadeOut(); }, 3000);
    }
  });

  // FORMULÁRIO DE CONTRATO
  $('#hiring-form').on('submit', function(e) {
    e.preventDefault();

    $('.msg-success').fadeIn();
    setTimeout(function() {
      $('.pricing-content').removeClass('opened-hiring');
      $('.hiring').removeClass('opened-hiring-box');
      $('.msg-success').fadeOut();
    }, 3000);

    var data = $(this).serialize();
    $.ajax({
      type: 'post',
      url: 'send-hiring.php',
      data: data,
      success: function(resp) {
        console.dir(resp);
      }
    });

    $('input, textarea').val('');

  });

})();
