(function () {
  'use strict';

  $('.slider-principal').cycle({
    fx: 'scrollHorz',
    timeout:7000,
    speed: 600,
    slides: '> figure',
    swipe: true,
    pager: '.pager-principal'
  });

	$('.slider').cycle({
		fx: 'scrollHorz',
		timeout:0,
		speed: 600,
		slides: '> figure',
		swipe: true,
		pager: '.pager'
	});

	$('.accordion a').click(function(j) {
		var dropDown = $(this).closest('li').find('p');

		$(this).closest('.accordion').find('p').not(dropDown).slideUp();

		if ($(this).hasClass('active')) {
				$(this).removeClass('active');
		} else {
				$(this).closest('.accordion').find('a.active').removeClass('active');
				$(this).addClass('active');
		}

		dropDown.stop(false, true).slideToggle();

		j.preventDefault();
	});

	// SMOOTH SCROOL
	$('header .nav a').click(function() {
			console.log('teste');
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	// Courses
	$('.block__course.informatica').on('click', function() {
		$('.lb-courses.informatica').fadeIn();
		$('body').addClass('no-scroll');
	});
	$('.block__course.gestao').on('click', function() {
		$('.lb-courses.gestao').fadeIn();
		$('body').addClass('no-scroll');
	});
	$('.block__course.enem').on('click', function() {
		$('.lb-courses.enem').fadeIn();
		$('body').addClass('no-scroll');
	});
	$('.block__course.livres').on('click', function() {
		$('.lb-courses.livres').fadeIn();
		$('body').addClass('no-scroll');
	});

	$('.back-courses').on('click', function() {
		$('.lb-courses.informatica').fadeOut();
		$('body').removeClass('no-scroll');
		$('html, body').animate({
			scrollTop: $('#cursos').offset().top
		}, 1000);	
	});
	$('.back-courses').on('click', function() {
		$('.lb-courses.gestao').fadeOut();
		$('body').removeClass('no-scroll');
		$('html, body').animate({
			scrollTop: $('#cursos').offset().top
		}, 1000);	
	});
	$('.back-courses').on('click', function() {
		$('.lb-courses.enem').fadeOut();
		$('body').removeClass('no-scroll');
		$('html, body').animate({
			scrollTop: $('#cursos').offset().top
		}, 1000);	
	});
	$('.back-courses').on('click', function() {
		$('.lb-courses.livres').fadeOut();
		$('body').removeClass('no-scroll');
		$('html, body').animate({
			scrollTop: $('#cursos').offset().top
		}, 1000);	
	});

	$('.swipebox').swipebox({
		hideBarsDelay : 0
	});

})();


//   $('.testimonials__content').cycle({
//     fx: 'scrollHorz',
//     timeout:7000,
//     speed: 600,
//     slides: '> .testimonial',
//     swipe: true,
//     pager: '#pagerTestimonial'
//   });

//   // BACK TOP
//   // $(".back-to-top").click(function(e) {
//   //   e.preventDefault();
//   //   $('html, body').animate({
//   //     scrollTop: $("#home").offset().top
//   //   }, 800);
//   // });

//   // SEND NEXT SECTION
//   $(".home__goDown").click(function(e) {
//     e.preventDefault();
//     $('html, body').animate({
//       scrollTop: $("#sobre").offset().top - 70
//     }, 800);
//   });


//   // FORMULÁRIO DE CONTATO
//   $('#form-contact').on('submit', function(e) {
//   	e.preventDefault();

//   	$(this).find('input[type="text"], textarea').each(function() {
//   		if ( $(this).val() == '' ){
//   			$(this).addClass('erro');
//   		}
//   	});

// 		if ( $(this).find('.erro').length == 0 ) {
//       $('.success').fadeIn();
//       $('.error').fadeOut();
//       setTimeout(function(){ $('.success').fadeOut(); }, 3000);

//     	var data = $(this).serialize();
// 			$.ajax({
// 				type: 'post',
// 				url: 'send.php',
// 				data: data,
// 				success: function(resp) {
// 					console.dir(resp);
// 				}
// 			});

//       $('input, textarea').val('');


//     } else {
//       $('.error').fadeIn();
//       $('.success').fadeOut();
//       setTimeout(function(){ $('.error').fadeOut(); }, 3000);      	
//     }
//   });

//   // FORMULÁRIO DE CONTRATAÇÃO
//   $('#form-hiring').on('submit', function(e) {
//   	e.preventDefault();

//   	$(this).find('input[type="text"], select').each(function() {
//   		if ( $(this).val() == '' ){
//   			$(this).addClass('erro');
//   		}
//   	});

// 		if ( $(this).find('.erro').length == 0 ) {
//       $('.success').fadeIn();
//       $('.error').fadeOut();
//       setTimeout(function(){ $('.success').fadeOut(); }, 3200);
//       setTimeout(function(){ $('.lb-hiring').fadeOut(); }, 3000);

//     	var data = $(this).serialize();
// 			$.ajax({
// 				type: 'post',
// 				url: 'send-hiring.php',
// 				data: data,
// 				success: function(resp) {
// 					console.dir(resp);
// 				}
// 			});

//       $('input, select').val('');


//     } else {
//       $('.error').fadeIn();
//       $('.success').fadeOut();
//       setTimeout(function(){ $('.error').fadeOut(); }, 3000);
//     }
//   });

//   // ESQUEMA DO HEADER
//   var headerDesktop = $(window).width();
//   if (headerDesktop <= 1000) {
//     $('#home').removeClass('desktop');
//   } else {
//     $('#home').addClass('desktop');
//   }

//   var nav = $('#sobre').offset().top - 200;
//   var header = $('#home').height();
//   $(window).scroll(function() {
//     var scroll = $(window).scrollTop();

//     if(scroll >= nav) {
//       $('#header').addClass('sticky');
//     } else {
//       $('#header').removeClass('sticky');
//     }
//   });

//   //LIGHTBOX PARA CONTRATAÇÃO
//   $('.plan__button').click(function (e) {
//     e.preventDefault();
//     $('.lb-hiring').fadeIn();
//     var planName = $(this).data("plan-name");

//     var checkBox = $(this).parent().find('.checkBox');

//     if(checkBox.is(':checked')) {
//       $('.plan-name-input').val(planName + ' + Base de e-mail');
//     } else {
//       $('.plan-name-input').val(planName);
//     }
//   });

//   $('.close-lb').click(function (e) {
//     e.preventDefault();
//     $('.lb-hiring').fadeOut();
//   });

//   $(document).bind('keydown', function(e) { 
//     if (e.which == 27) {
//       $('.lb-hiring').fadeOut();
//     }
//   });


//   $('.checkBox').click(function() {
//     var currentPrice = $(this).data('current-price');
//     var basePrice = $(this).data('base-price');

//     if ( $(this).is(':checked') ) {
//       console.log(basePrice);
//       $(this).parent().parent().find('.plan__amount__base span').removeClass('opacity');
//       $(this).parent().parent().find('.item-price').html(basePrice);

//     } else {
//       console.log(currentPrice);
//       $(this).parent().parent().find('.plan__amount__base span').addClass('opacity');
//       $(this).parent().parent().find('.item-price').html(currentPrice);
//     }
//   });