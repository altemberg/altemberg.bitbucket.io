(function () {
  'use strict';

	// SMOOTH SCROOL
	$('header .nav a').click(function() {
			console.log('teste');
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});


})();


	$('.box-testimonials__items').cycle({
	  fx: 'scrollHorz',
	  timeout:7000,
	  speed: 600,
	  slides: '> .item',
	  swipe: true,
	  pager: '#pagerTestimonial'
	});

	$('.box-faq__questions-item-title').on('click', function(){
		if( $(this).hasClass('opened') ) {
			$(this).removeClass('opened');
			$(this).parent().find('.box-faq__questions-item-content').hide();
		} else {
			$(this).addClass('opened');
			// $(this).parent().find('box-faq__questions-item-content').show();
			$(this).parent().find('.box-faq__questions-item-content').show();
		}
	});


//   // BACK TOP
//   // $(".back-to-top").click(function(e) {
//   //   e.preventDefault();
//   //   $('html, body').animate({
//   //     scrollTop: $("#home").offset().top
//   //   }, 800);
//   // });

//   // SEND NEXT SECTION
//   $(".home__goDown").click(function(e) {
//     e.preventDefault();
//     $('html, body').animate({
//       scrollTop: $("#sobre").offset().top - 70
//     }, 800);
//   });


//   // FORMULÁRIO DE CONTATO
//   $('#form-contact').on('submit', function(e) {
//   	e.preventDefault();

//   	$(this).find('input[type="text"], textarea').each(function() {
//   		if ( $(this).val() == '' ){
//   			$(this).addClass('erro');
//   		}
//   	});

// 		if ( $(this).find('.erro').length == 0 ) {
//       $('.success').fadeIn();
//       $('.error').fadeOut();
//       setTimeout(function(){ $('.success').fadeOut(); }, 3000);

//     	var data = $(this).serialize();
// 			$.ajax({
// 				type: 'post',
// 				url: 'send.php',
// 				data: data,
// 				success: function(resp) {
// 					console.dir(resp);
// 				}
// 			});

//       $('input, textarea').val('');


//     } else {
//       $('.error').fadeIn();
//       $('.success').fadeOut();
//       setTimeout(function(){ $('.error').fadeOut(); }, 3000);      	
//     }
//   });

//   // FORMULÁRIO DE CONTRATAÇÃO
//   $('#form-hiring').on('submit', function(e) {
//   	e.preventDefault();

//   	$(this).find('input[type="text"], select').each(function() {
//   		if ( $(this).val() == '' ){
//   			$(this).addClass('erro');
//   		}
//   	});

// 		if ( $(this).find('.erro').length == 0 ) {
//       $('.success').fadeIn();
//       $('.error').fadeOut();
//       setTimeout(function(){ $('.success').fadeOut(); }, 3200);
//       setTimeout(function(){ $('.lb-hiring').fadeOut(); }, 3000);

//     	var data = $(this).serialize();
// 			$.ajax({
// 				type: 'post',
// 				url: 'send-hiring.php',
// 				data: data,
// 				success: function(resp) {
// 					console.dir(resp);
// 				}
// 			});

//       $('input, select').val('');


//     } else {
//       $('.error').fadeIn();
//       $('.success').fadeOut();
//       setTimeout(function(){ $('.error').fadeOut(); }, 3000);
//     }
//   });

//   // ESQUEMA DO HEADER
//   var headerDesktop = $(window).width();
//   if (headerDesktop <= 1000) {
//     $('#home').removeClass('desktop');
//   } else {
//     $('#home').addClass('desktop');
//   }

//   var nav = $('#sobre').offset().top - 200;
//   var header = $('#home').height();
//   $(window).scroll(function() {
//     var scroll = $(window).scrollTop();

//     if(scroll >= nav) {
//       $('#header').addClass('sticky');
//     } else {
//       $('#header').removeClass('sticky');
//     }
//   });

//   //LIGHTBOX PARA CONTRATAÇÃO
//   $('.plan__button').click(function (e) {
//     e.preventDefault();
//     $('.lb-hiring').fadeIn();
//     var planName = $(this).data("plan-name");

//     var checkBox = $(this).parent().find('.checkBox');

//     if(checkBox.is(':checked')) {
//       $('.plan-name-input').val(planName + ' + Base de e-mail');
//     } else {
//       $('.plan-name-input').val(planName);
//     }
//   });

//   $('.close-lb').click(function (e) {
//     e.preventDefault();
//     $('.lb-hiring').fadeOut();
//   });

//   $(document).bind('keydown', function(e) { 
//     if (e.which == 27) {
//       $('.lb-hiring').fadeOut();
//     }
//   });


//   $('.checkBox').click(function() {
//     var currentPrice = $(this).data('current-price');
//     var basePrice = $(this).data('base-price');

//     if ( $(this).is(':checked') ) {
//       console.log(basePrice);
//       $(this).parent().parent().find('.plan__amount__base span').removeClass('opacity');
//       $(this).parent().parent().find('.item-price').html(basePrice);

//     } else {
//       console.log(currentPrice);
//       $(this).parent().parent().find('.plan__amount__base span').addClass('opacity');
//       $(this).parent().parent().find('.item-price').html(currentPrice);
//     }
//   });